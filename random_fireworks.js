// --------------------------------------------------------------------------------------
// 各スクリプト共通部
// --------------------------------------------------------------------------------------

// 要素の取得
var elemContainer = document.getElementById("container");
var elemCanvasArea = document.getElementById("canvasArea");

// CanvasRenderingContext2Dの取得
// 図形、テキスト、画像、その他のオブジェクトの描画に使用されます。
var ctx = elemCanvasArea.getContext("2d");

// style属性のwidth, heightを
// <canvas>タグの座標空間のwidth, heightとして設定
elemCanvasArea.width = elemCanvasArea.clientHeight;
elemCanvasArea.height = elemCanvasArea.clientWidth;

// start~endまでの乱数を返す関数
function makeRangeRandom(start, end) {
  return Math.random() * (end - start) + start;
}

// ランダムなrgbの文字列を生成
function rgb() {
  var r = makeRangeRandom(0, 255);
  var g = makeRangeRandom(0, 255);
  var b = makeRangeRandom(0, 255);
  return "rgb(" + r + ", " + g + ", " + b + ")";
}


// ランダムなrgbの文字列を生成
function rgba() {
  var r = makeRangeRandom(0, 255);
  var g = makeRangeRandom(0, 255);
  var b = makeRangeRandom(0, 255);
  var a = Math.random();
  return "rgba(" + r + ", " + g + ", " + b + ", " + a + ")";
}

// ランダムなrgbの文字列を生成
function rgbax() {
  var r = makeRangeRandom(30, 50);
  var g = makeRangeRandom(30, 50);
  var b = makeRangeRandom(30, 50);
  var a = Math.random();
  return "rgba(" + r + ", " + g + ", " + b + ", " + a + ")";
}


// cycleCount回数分、intervalMs間隔で、callbackを実行する関数
function loopInterval(cycleCount, intervalMs, callback) {
  var elapsedCount = 0;

  var timerId = setInterval(function() {

    if (cycleCount > elapsedCount) {
      callback();
      elapsedCount++;
    } else {
      clearInterval(timerId);
    }

  }, intervalMs);
}

// --------------------------------------------------------------------------------------
// 各スクリプト固有部
// --------------------------------------------------------------------------------------

// 1/2の確立で1、1/2の確率で-1を返す
function noise() {
  return Math.random() >= 0.5 ? 1 : -1;
}

// 座標の移動位置
var X_HALF = elemCanvasArea.width / 2;
var Y_HALF = elemCanvasArea.height / 2;

var DATA_COUNT = 100;
var DATA_COUNTX = 5;

var draw = (function() {

  var tensAll = Array(DATA_COUNT);
  for (var i = 0; i < DATA_COUNT; i++) {

    var tens = Array(DATA_COUNTX);
    for (var a = 0; a < DATA_COUNTX; a++) {
      // distance: どのくらいまで進んだら曲がるのか。
      // angle: どのくらいの角度で曲がるのか。

      tens[a] = {
        "x": Math.random() * 999 ,
        "y": Math.random() * 999
      }
    }

    tensAll[i] = tens;
  }

  ctx.lineWidth = 4;

  for (var a = 0; a < tensAll.length; a++) {
    ctx.lineCap = "square";

    ctx.beginPath();
    ctx.moveTo(X_HALF, Y_HALF);

    var tens = tensAll[a];

    for (var i = 0; i < tens.length; i++) {
      var ten = tens[i];

      var totalX = 0;
      var totalY = 0;

      totalX += ten.x;
      totalY += ten.y;

      ctx.strokeStyle = "#447EC1";

      ctx.shadowBlur  = 4;      //ぼかしを０にする
      ctx.shadowOffsetX = 3;    //横に3pxずらす
      ctx.shadowOffsetY = 3;    //縦に1pxずらす
      //ctx.quadraticCurveTo(totalX / 2, totalY / 2, totalX, totalY);
      ctx.bezierCurveTo(totalX / (Math.random()*3), totalY / (Math.random()*3), totalX / (Math.random()*3), totalY / (Math.random()*3), totalX, totalY);
      // ctx.lineTo(totalX, totalY);
    }

    ctx.closePath();

    ctx.stroke();
  }

});

loopInterval(1, 50, draw);
