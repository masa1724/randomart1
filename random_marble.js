// --------------------------------------------------------------------------------------
// 各スクリプト共通部
// --------------------------------------------------------------------------------------

// 要素の取得
var elemContainer = document.getElementById("container");
var elemCanvasArea = document.getElementById("canvasArea");

// CanvasRenderingContext2Dの取得
// 図形、テキスト、画像、その他のオブジェクトの描画に使用されます。
var ctx = elemCanvasArea.getContext("2d");

// style属性のwidth, heightを
// <canvas>タグの座標空間のwidth, heightとして設定
elemCanvasArea.width = elemCanvasArea.clientHeight;
elemCanvasArea.height = elemCanvasArea.clientWidth;

// start~endまでの乱数を返す関数
function makeRangeRandom(start, end) {
  return Math.random() * (end - start) + start;
}

// ランダムなrgbの文字列を生成
function rgb() {
  var r = makeRangeRandom(0, 255);
  var g = makeRangeRandom(0, 255);
  var b = makeRangeRandom(0, 255);
  return "rgb(" + r + ", " + g + ", " + b + ")";
}

// cycleCount回数分、intervalMs間隔で、callbackを実行する関数
function loopInterval(cycleCount, intervalMs, callback) {
  var elapsedCount = 0;

  var timerId = setInterval(function() {

    if (cycleCount > elapsedCount) {
      callback();
      elapsedCount++;
    } else {
      clearInterval(timerId);
    }

  }, intervalMs);
}

// --------------------------------------------------------------------------------------
// 各スクリプト固有部
// --------------------------------------------------------------------------------------

// 1/2の確立で1、1/2の確率で-1を返す
function noise() {
  return Math.random() >= 0.5 ? 1 : -1;
}

// 座標の移動位置
var X_HALF = elemCanvasArea.width / 2;
var Y_HALF = elemCanvasArea.height / 2;

// 座標端の余白
var XY_PADDING = 5;

// データ数
var DATA_COUNT = 1000;
// 円の半径
var RADIUS = 10;
// 移動距離
var MOVE_VAL = 10;
// 最低移動距離
var MOVE_VAL_MIN = 50;

// ランダム値を作成
var datas = Array(DATA_COUNT);
for (var i = 0; i < datas.length; i++) {
  datas[i] = {
    // 円の半径
    "radius": RADIUS,
    // 円の中心から開始させる
    "x": X_HALF,
    "y": Y_HALF,
    // 移動先の座標
    "toX": X_HALF + (noise() * makeRangeRandom(MOVE_VAL_MIN, X_HALF)),
    "toY": Y_HALF + (noise() * makeRangeRandom(MOVE_VAL_MIN, Y_HALF)),
    "moveX": makeRangeRandom(0, MOVE_VAL),
    "moveY":  makeRangeRandom(0, MOVE_VAL),
    // 円の色
    "color": rgb()
  };
}

// 線の太さ
ctx.lineWidth = 1;

var draw = (function() {
  // 現在の描画をクリア
  ctx.clearRect(0, 0, elemCanvasArea.width, elemCanvasArea.height);

  // 描画
  for (var i = 0; i < datas.length; i++) {
    var data = datas[i];

    // 線の色
    ctx.strokeStyle = data.color;
    ctx.fillStyle = data.color;
    ctx.shadowColor ="#444";
    ctx.shadowOffsetX = 2;
    ctx.shadowOffsetY = 2;
    ctx.shadowBlur = 3;

    // パスをリセット
    ctx.beginPath();

    // 円弧の中心のy座標値。
    // 円弧の半径。
    // 円弧の始まりの角度。x軸の正方向から時計回りに定められるラジアン角。
    // 円弧の終わりの角度。x軸の正方向から時計回りに定められるラジアン角。
    // 方向: true=反時計回りの円、false=時計回りの円
    ctx.arc(
      data.x,
      data.y,
      data.radius,
      0,
      2 * Math.PI,
      false
    );

    // 描画
    ctx.fill();
    ctx.stroke();

    // 0~half~width
    // 0~half~height
    // y:正 half→0
    // y:負 half→height
    // x:正 half→width
    // x:負 half→0

    // 残りの移動距離
    var dictanceX = Math.abs(data.x - data.toX);
    var dictanceY = Math.abs(data.y - data.toY);

    if (dictanceX >= XY_PADDING && dictanceY >= XY_PADDING) {
      // 進行方向
      // true: →, false: ←
      var directionX = data.toX >= X_HALF;
      // true: ↓, false: ↑
      var directionY = data.toY >= Y_HALF;

      // 次回描画用に移動させる
      data.x += directionX ? data.moveX : -data.moveX;
      data.y += directionY ? data.moveY  : -data.moveY;
    }
  }
});

loopInterval(200, 50, draw);
