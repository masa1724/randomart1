// --------------------------------------------------------------------------------------
// 各スクリプト共通部
// --------------------------------------------------------------------------------------

// 要素の取得
var elemContainer = document.getElementById("container");
var elemCanvasArea = document.getElementById("canvasArea");

// CanvasRenderingContext2Dの取得
// 図形、テキスト、画像、その他のオブジェクトの描画に使用されます。
var ctx = elemCanvasArea.getContext("2d");

// style属性のwidth, heightを
// <canvas>タグの座標空間のwidth, heightとして設定
elemCanvasArea.width = elemCanvasArea.clientHeight;
elemCanvasArea.height = elemCanvasArea.clientWidth;

// start~endまでの乱数を返す関数
function makeRangeRandom(start, end) {
  return Math.random() * (end - start) + start;
}

// ランダムなrgbの文字列を生成
function rgb() {
  var r = makeRangeRandom(0, 255);
  var g = makeRangeRandom(0, 255);
  var b = makeRangeRandom(0, 255);
  return "rgb(" + r + ", " + g + ", " + b + ")";
}

// cycleCount回数分、intervalMs間隔で、callbackを実行する関数
function loopInterval(cycleCount, intervalMs, callback) {
  var elapsedCount = 0;

  var timerId = setInterval(function() {

    if (cycleCount > elapsedCount) {
      callback();
      elapsedCount++;
    } else {
      clearInterval(timerId);
    }

  }, intervalMs);
}

// --------------------------------------------------------------------------------------
// 各スクリプト固有部
// --------------------------------------------------------------------------------------

// 線の太さ
ctx.lineWidth = 1;

var func = (function() {
  // 円の半径
  var radius = 20;

  // 線の色
  ctx.strokeStyle = rgb();

  // パスをリセット
  ctx.beginPath();

  // 円弧の中心のx座標値。
  // 円弧の中心のy座標値。
  // 円弧の半径。
  // 円弧の始まりの角度。x軸の正方向から時計回りに定められるラジアン角。
  // 円弧の終わりの角度。x軸の正方向から時計回りに定められるラジアン角。
  // 方向: true=反時計回りの円、false=時計回りの円
  ctx.arc(
    elemCanvasArea.width * (Math.random() * 10) / 2,
    elemCanvasArea.height * (Math.random() * 10) / 2,
    radius,
    0,
    2 * Math.PI,
    false
  );

  // 線を描画を実行
  ctx.stroke();

  radius++; // 次にサイズを1大きくする
});

// 1000回実行
loopInterval(1000, 200, func);

function r() {
  var random = (Math.random() + Math.random() + Math.random() + Math.random() + Math.random());
  return random;
}

function r2() {
  var random2 = (Math.random() + Math.random() + Math.random() + Math.random() + Math.random()) * 3;
  return random2;
}

function rgb() {
  var r = makeRangeRandom(0, 255);
  var g = makeRangeRandom(0, 255);
  var b = makeRangeRandom(0, 255);
  return "rgb(" + r + ", " + g + ", " + b + ")";
}

//引数にはミリ秒を指定します。（例：5秒の場合は5000）
function sleep(a) {
  var dt1 = new Date().getTime();
  var dt2 = new Date().getTime();
  while (dt2 < dt1 + a) {
    dt2 = new Date().getTime();
  }
  return;
}
