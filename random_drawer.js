// --------------------------------------------------------------------------------------
// 各スクリプト共通部
// --------------------------------------------------------------------------------------

// 要素の取得
var elemContainer = document.getElementById("container");
var elemCanvasArea = document.getElementById("canvasArea");

// CanvasRenderingContext2Dの取得
// 図形、テキスト、画像、その他のオブジェクトの描画に使用されます。
var ctx = elemCanvasArea.getContext("2d");

// style属性のwidth, heightを
// <canvas>タグの座標空間のwidth, heightとして設定
elemCanvasArea.width = elemCanvasArea.clientHeight;
elemCanvasArea.height = elemCanvasArea.clientWidth;

// start~endまでの乱数を返す関数
function makeRangeRandom(start, end) {
  return Math.random() * (end - start) + start;
}

// ランダムなrgbの文字列を生成
function rgb() {
  var r = makeRangeRandom(0, 255);
  var g = makeRangeRandom(0, 255);
  var b = makeRangeRandom(0, 255);
  return "rgb(" + r + ", " + g + ", " + b + ")";
}

// ランダムなrgbの文字列を生成
function rgba() {
  var r = makeRangeRandom(0, 255);
  var g = makeRangeRandom(0, 255);
  var b = makeRangeRandom(0, 255);
  var a = 0.3;
  return "rgba(" + r + ", " + g + ", " + b + ", " + a + ")";
}

// cycleCount回数分、intervalMs間隔で、callbackを実行する関数
function loopInterval(cycleCount, intervalMs, callback) {
  var elapsedCount = 0;

  var timerId = setInterval(function() {

    if (cycleCount > elapsedCount) {
      callback();
      elapsedCount++;
    } else {
      clearInterval(timerId);
    }

  }, intervalMs);
}

// --------------------------------------------------------------------------------------
// 各スクリプト固有部
// --------------------------------------------------------------------------------------

// 1/2の確立で1、1/2の確率で-1を返す
function noise() {
  return Math.random() >= 0.5 ? 1 : -1;
}

//---------------------------------------------------------------
// 定数定義
//---------------------------------------------------------------

// 座標の移動位置
var X_FULL = elemCanvasArea.width;
var Y_FULL = elemCanvasArea.height;
var X_HALF = X_FULL / 2;
var Y_HALF = Y_FULL / 2;

// 円の半径
  var RADIUS = 3;
// 移動距離
var MOVE_VAL_MIN = 1;
var MOVE_VAL_MAX = 1;

// データの個数
var ROOT_COUNT = 10;
var NODE_COUNT = 12;

// 塗りつぶし有無
var IS_FILL = false;

// 背景色
var BACK_GROUND_COLOR = "#4A8BC9";

//---------------------------------------------------------------
//  初期データの作成
//---------------------------------------------------------------

var roots = Array(ROOT_COUNT);
for (var i = 0; i < roots.length; i++) {
  var root = {
    "x": X_HALF,
    "y": Y_HALF,
    "fillColor": "#4A8BC9",
    "lineColor": "rgba(255,255,255,0.5)",
    "nodes": []
  };

  var nodes = Array(NODE_COUNT);
  for (var y = 0; y < nodes.length; y++) {
    nodes[y] = {
      "x": X_HALF,
      "y": Y_HALF
    }
  }

  root.nodes = nodes;
  roots[i] = root;
}

ctx.lineWidth = 1;


//---------------------------------------------------------------
//  移動、描画処理
//---------------------------------------------------------------

var draw = (function() {
  // 現在の描画をクリア
  ctx.clearRect(0, 0, X_FULL, Y_FULL);

  // 背景色の描画
  ctx.fillStyle = BACK_GROUND_COLOR;
  ctx.fillRect(0, 0, X_FULL, Y_FULL);

  // 描画
  for (var i = 0; i < roots.length; i++) {
    var root = roots[i];

    var rootMoveX = noise() * makeRangeRandom(MOVE_VAL_MIN, MOVE_VAL_MAX);
    var rootMoveY = noise() * makeRangeRandom(MOVE_VAL_MIN, MOVE_VAL_MAX);
    root.x += rootMoveX;
    root.y += rootMoveY;

    // 線の色
    ctx.strokeStyle = root.lineColor;
    ctx.fillStyle = root.fillColor;

    // パスをリセット
    ctx.beginPath();

    for (var y = 0; y < root.nodes.length; y++) {
      console.log("bbb");

      var node = root.nodes[y];

      var nodeMoveX = noise() * makeRangeRandom(MOVE_VAL_MIN, MOVE_VAL_MAX) + rootMoveX;
      var nodeMoveY = noise() * makeRangeRandom(MOVE_VAL_MIN, MOVE_VAL_MAX) + rootMoveY;
      node.x += nodeMoveX;
      node.y += nodeMoveY;

      ctx.arc(
        node.x,
        node.y,
        RADIUS,
        0,
        2 * Math.PI,
        false
      );
    }

    ctx.closePath();

    // 描画
    if (IS_FILL) ctx.fill();
    ctx.stroke();
  }
});

loopInterval(10000, 5, draw);
